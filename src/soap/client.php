<?php
ini_set("soap.wsdl_cache_enabled", "0");
$client = new SoapClient(__DIR__ . "/stockquote.wsdl");
  try {  
    echo "SOAP request getQuote(\"ibm\")"  . PHP_EOL;
    print($client->getQuote("ibm"));  
    echo PHP_EOL . PHP_EOL;

    echo "SOAP request getQuote(\"microsoft\")"  . PHP_EOL;
    print($client->getQuote("microsoft"));    
    echo PHP_EOL;
  } catch (SoapFault $exception) {
    echo "Catch exception from SOAP: $exception" . PHP_EOL;
  }  

<?php  
class QuoteService {  
  private $quotes = ["ibm" => 98.42];

  function getQuote($symbol) {  
    if (isset($this->quotes[$symbol])) {  
      return $this->quotes[$symbol];  
    } else {  
      throw new SoapFault("Hi from SOAP Server","Unknown Symbol '$symbol'.");
    }  
  }  
}  

$server = new SoapServer(__DIR__ . "/stockquote.wsdl");
$server->setClass("QuoteService");  
$server->handle();  
 

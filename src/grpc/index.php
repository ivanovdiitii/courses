<?php
require_once "vendor/autoload.php";
// Full gRPC method name in format:
// package.service-name/method-name
$route = $_GET['r'];
//echo "START" . PHP_EOL;
// Protobuf-serialized request message body
$body = file_get_contents("php://input");

try {
	$request = new \Api\Customer\GetSomeInfoRequest();
	$request->parseFromStream(new \Google\Protobuf\Internal\CodedInputStream($body));
	$response = new \Api\Customer\GetSomeInfoResponse();

	$response->setFirstName("Test Testov " . $request->getLogin());

	echo $response->serializeToString();
} catch (\Throwable $e) {
	// https://github.com/grpc/grpc-go/blob/master/codes/codes.go
	$errorCode = 13;
	header("X-Grpc-Status: ERROR");
	header("X-Grpc-Error-Code: {$errorCode}");
	header("X-Grpc-Error-Description: {$e->getMessage()}");
}
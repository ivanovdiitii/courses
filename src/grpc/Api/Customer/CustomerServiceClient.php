<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Api\Customer;

/**
 */
class CustomerServiceClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * @param \Api\Customer\GetSomeInfoRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function getSomeInfo(\Api\Customer\GetSomeInfoRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/api.customer.CustomerService/getSomeInfo',
        $argument,
        ['\Api\Customer\GetSomeInfoResponse', 'decode'],
        $metadata, $options);
    }

}

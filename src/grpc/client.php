<?php
//php -d extension=grpc.so ./client.php
//protoc --proto_path=./ --php_out=./ --grpc_out=./ --plugin=protoc-gen-grpc=/application/grpc/bins/opt/grpc_php_plugin ./test.proto

require_once "vendor/autoload.php";

$client = new \Api\Customer\CustomerServiceClient('webserver:8080', [
	'credentials' => Grpc\ChannelCredentials::createInsecure(),
]);

$someInfoRequest = new \Api\Customer\GetSomeInfoRequest();
$someInfoRequest->setLogin("Login " .  rand(10, 100));
list($reply, $status) =$client->getSomeInfo($someInfoRequest)->wait();

/**
 * @var $reply Api\Customer\GetSomeInfoResponse
 */
echo $reply->getFirstName() . PHP_EOL;
echo $reply->getSecondName() . PHP_EOL;
//echo $message . ' ' . $status . PHP_EOL;

echo "DONE" . PHP_EOL;
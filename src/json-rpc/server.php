<?php
use Datto\JsonRpc\Server;

require_once __DIR__ . '/vendor/autoload.php';
$server = new Server(new JRPC\Calculator());
$request = file_get_contents('php://input');

$reply = $server->reply($request);
echo $reply, "\n";
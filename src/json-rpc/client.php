<?php

use Datto\JsonRpc\Client;
require_once __DIR__ . '/vendor/autoload.php';

$client = new Client();
$client->query(1, 'add', array(1, 2));
$client->query(2, 'add', array('a', 'b'));
$client->query(3, 'add', array(5, 2));
$message = $client->encode();
echo "Sended queries:\n{$message}\n\n";

$guzzle = new GuzzleHttp\Client();
$guzzle_reply = $guzzle->post('http://webserver/jrpc/', ['body' => $message]);

$reply = $guzzle_reply->getBody();
//'[{"jsonrpc":"2.0","id":1,"result":3},{"jsonrpc":"2.0","id":2,"error":{"code":-32602,"message":"Invalid params"}}]';
echo "Response $reply" . PHP_EOL;

$responses = $client->decode($reply);

echo "Parsed response:\n";
foreach ($responses as $response) {
	$id = $response->getId();
	$isError = $response->isError();
	if ($isError) {
		$error = $response->getError();
		$errorProperties = array(
			'code' => $error->getCode(),
			'message' => $error->getMessage(),
			'data' => $error->getData()
		);
		echo " * id: {$id}, error: ", json_encode($errorProperties), "\n";
	} else {
		$result = $response->getResult();
		echo " * id: {$id}, result: ", json_encode($result), "\n";
	}
}
echo "\n";